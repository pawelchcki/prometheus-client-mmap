#ifndef MMAP_H
#define MMAP_H

#define MM_MODIFY 1
#define MM_ORIGIN 2
#define MM_CHANGE (MM_MODIFY | 4)
#define MM_PROTECT 8

#define MM_FROZEN (1 << 0)
#define MM_FIXED (1 << 1)
#define MM_ANON (1 << 2)
#define MM_LOCK (1 << 3)
#define MM_IPC (1 << 4)
#define MM_TMP (1 << 5)

#ifndef MMAP_RETTYPE
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309
#endif /* !_POSIX_C_SOURCE */
#ifdef _POSIX_VERSION
#if _POSIX_VERSION >= 199309
#define MMAP_RETTYPE void *
#endif /* _POSIX_VERSION >= 199309 */
#endif /* _POSIX_VERSION */
#endif /* !MMAP_RETTYPE */

#ifndef MMAP_RETTYPE
#define MMAP_RETTYPE caddr_t
#endif

typedef struct {
    MMAP_RETTYPE addr;
    int smode, pmode, vscope;
    int advice, flag;
    VALUE key;
    int semid, shmid;
    size_t len, real, incr;
    off_t offset;
    char *path, *template;
} mm_mmap;

typedef struct {
    int count;
    mm_mmap *t;
} mm_ipc;


#define GET_MMAP(obj, i_mm, t_modify)                             \
    Data_Get_Struct(obj, mm_ipc, i_mm);                          \
    if (!i_mm->t->path) {                                        \
        rb_raise(rb_eIOError, "unmapped file");                  \
    }                                                            \
    if ((t_modify & MM_MODIFY) && (i_mm->t->flag & MM_FROZEN)) { \
        rb_error_frozen("mmap");                                 \
    }

#endif
