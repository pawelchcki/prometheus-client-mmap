require 'mkmf'
$CFLAGS << ' -std=c99 -Wall -Wextra -Werror'

CONFIG['warnflags'].slice!(/ -Wdeclaration-after-statement/)

dir_config('fast_mmaped_file')
create_makefile('fast_mmaped_file')
