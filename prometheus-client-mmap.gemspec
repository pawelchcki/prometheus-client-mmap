#encoding: utf-8
$LOAD_PATH.push File.expand_path('../lib', __FILE__)
require 'prometheus/client/version'
require 'rake'

Gem::Specification.new do |s|
  s.name              = 'prometheus-client-mmap'
  s.version           = Prometheus::Client::VERSION
  s.summary           = 'A suite of instrumentation metric primitives' \
                        'that can be exposed through a web services interface.'
  s.authors           = ['Tobias Schmidt', 'Paweł Chojnacki']
  s.email             = ['ts@soundcloud.com', 'pawel@gitlab.com']
  s.homepage          = 'https://gitlab.com/gitlab-org/prometheus-client-mmap'
  s.license           = 'Apache 2.0'

  s.files             = %w(README.md) + Dir.glob('{lib/**/*}') + Dir.glob('{ext/**/*}')
  s.require_paths     = ['lib']
  s.extensions        = FileList['ext/**/extconf.rb']

  s.add_runtime_dependency 'mmap2', '~> 2.2', '>= 2.2.9'
  s.add_development_dependency 'fuzzbert', '~> 1.0', '>= 1.0.4'
  s.add_development_dependency 'gem_publisher', '~> 1.1.1'
  s.add_development_dependency 'rake-compiler', '~> 1.0.4'
  s.add_development_dependency 'ruby-prof', '~> 0.16.2'
end
