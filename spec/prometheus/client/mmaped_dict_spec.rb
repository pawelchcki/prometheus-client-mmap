require 'prometheus/client/mmaped_dict'
require 'tempfile'

describe Prometheus::Client::MmapedDict do
  let(:tmp_file) { Tempfile.new('mmaped_dict') }
  let(:tmp_mmaped_file) { Prometheus::Client::Helper::MmapedFile.open(tmp_file) }

  after do
    tmp_mmaped_file.close
    tmp_file.close
    tmp_file.unlink
  end

  describe '#initialize' do
    describe "empty mmap'ed file" do
      it 'is initialized with correct size' do
        described_class.new(tmp_mmaped_file)

        expect(File.size(tmp_file.path)).to eq(tmp_mmaped_file.send(:initial_mmap_file_size))
      end
    end

    describe "mmap'ed file that is above minimum size" do
      let(:above_minimum_size) { Prometheus::Client::Helper::EntryParser::MINIMUM_SIZE + 1 }

      before do
        tmp_file.truncate(above_minimum_size)
      end

      it 'is initialized with the same size' do
        described_class.new(tmp_mmaped_file)

        tmp_file.open
        expect(tmp_file.size).to eq(above_minimum_size)
      end
    end
  end

  describe 'read and write values' do
    let(:locked_file) { Prometheus::Client::Helper::MmapedFile.ensure_exclusive_file }
    let(:mmaped_file) { Prometheus::Client::Helper::MmapedFile.open(locked_file) }

    before do
      Prometheus::Client.configuration.multiprocess_files_dir = Dir.tmpdir

      data = described_class.new(Prometheus::Client::Helper::MmapedFile.open(locked_file))
      data.write_value('foo', 100)
      data.write_value('bar', 500)

      data.close
    end

    after do
      mmaped_file.close
      Prometheus::Client::Helper::FileLocker.unlock(locked_file)
      File.unlink(locked_file)
    end

    it '#read_all_values' do
      values = described_class.read_all_values(locked_file)

      expect(values.count).to eq(2)
      expect(values[0]).to eq(['foo', 100])
      expect(values[1]).to eq(['bar', 500])
    end

    it '#read_all_positions' do
      data = described_class.new(Prometheus::Client::Helper::MmapedFile.open(locked_file))

      positions = data.positions

      # Generated via https://github.com/luismartingarcia/protocol:
      # protocol "Used:4,Pad:4,K1 Size:4,K1 Name:4,K1 Value:8,K2 Size:4,K2 Name:4,K2 Value:8"
      #
      # 0                   1                   2                   3
      # 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      # +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      # |  Used |  Pad  |K1 Size|K1 Name|   K1 Value    |K2 Size|K2 Name|
      # +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      # |  K2 Value   |
      # +-+-+-+-+-+-+-+
      expect(positions).to eq({ 'foo' => 16, 'bar' => 32 })
    end

    it '#write_value' do
      # Reload dictionary
      data = described_class.new(mmaped_file)
      data.write_value('new value', 500)
      # Overwrite existing values
      data.write_value('foo', 200)
      data.write_value('bar', 300)

      values = described_class.read_all_values(locked_file)

      expect(values.count).to eq(3)

      expect(values[0]).to eq(['foo', 200])
      expect(values[1]).to eq(['bar', 300])
      expect(values[2]).to eq(['new value', 500])
    end
  end
end
