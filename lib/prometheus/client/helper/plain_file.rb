require 'prometheus/client/helper/entry_parser'

module Prometheus
  module Client
    module Helper
      # Parses DB files without using mmap
      class PlainFile
        include EntryParser
        attr_reader :filepath

        def initialize(filepath)
          @filepath = filepath
          @data = File.read(filepath, mode: 'rb')
        end

        def slice(*args)
          @data.slice(*args)
        end

        def size
          @data.length
        end
      end
    end
  end
end
